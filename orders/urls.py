from django.urls import path

from orders import views

app_name = 'orders'
urlpatterns = [

    path('order_list', views.order_list, name='order_list'),
    path('add_order/<int:id_client>/<int:id_order>', views.add_order, name='add_order'),
    path('checkout', views.CheckOut, name='check_out'),

]
