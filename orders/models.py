from django.utils import timezone

from django.db import models
from users.models import User


class Order(models.Model):
    id_user = models.IntegerField(default=0)
    dish_name = models.CharField(max_length=200, blank=True, null=True)
    dish_price = models.DecimalField(blank=True, null=True, decimal_places=3, max_digits=10)
    dish_description = models.TextField(blank=True, null=True)
    date = models.DateTimeField(auto_now=True)
    status = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.id_user}, {self.dish_name}, {self.dish_price}'
