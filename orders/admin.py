from django.contrib import admin

# Register your models here.
from orders.models import Order


@admin.register(Order)
class UserAdmin(admin.ModelAdmin):
    list_display = ('id', 'id_user', 'dish_name', 'dish_price', 'date', 'status')
