from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import render, redirect


from .models import Order



def CheckOut(request):
    """
    Delete a pet changing his status account to False
    """
    total = 0
    orders = Order.objects.filter(status=True)
    for order in orders:
        total = total + order.dish_price

    context = {
        'total':total
    }
    for order in orders:
        order.status = False
        order.id_user = 0
        order.save()


    return render(request,'order/checkout_page.html',context)

def add_order(request, id_client, id_order):
    order = Order.objects.get(id=id_order)
    order.status = True
    order.id_user = id_client
    order.save()
    orders = Order.objects.filter(status=True)
    total = 0
    for order in orders:
        total = total + order.dish_price

    context = {
        'orders': orders,
        'total': total
    }
    return render(request, 'order/order_complete_list.html', context)



@login_required
def order_list(request):
    """
    You can show your pets of your database
    """
    orders = Order.objects.filter(status=True)
    total = 0
    for order in orders:
        total = total + order.dish_price

    context = {
        'orders': orders,
        'total': total
    }

    return render(request, 'order/order_complete_list.html', context)