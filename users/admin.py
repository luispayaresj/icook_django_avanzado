from django.contrib import admin

from users.models import Client


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'cell_phone', 'address')
    search_fields = ('id', 'user')
