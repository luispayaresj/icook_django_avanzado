from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import render
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView, FormView, CreateView, RedirectView

from users.forms import RegisterForm, LoginForm


class HomeView(TemplateView):
    """
        This view shows the home page of the app.
    """
    template_name = "base.html"


class RegisterView(CreateView):
    form_class = RegisterForm
    model = User
    template_name = 'registration/register.html'
    success_url = reverse_lazy('users:home')


class LoginView(FormView):
    form_class = LoginForm
    template_name = 'registration/login.html'
    success_url = reverse_lazy('users:home')

    def form_valid(self, form):
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user = authenticate(username=username, password=password)

        if user is not None and user.is_active:
            login(self.request, user)
            return super(LoginView, self).form_valid(form)
        else:
            return self.form_invalid(form)


class OnlineOrderPageView(TemplateView):
    """
        This view shows the home page of the app.
    """
    template_name = "order/order.html"


class LogoutView(RedirectView):
    url = reverse_lazy('users:login')

    def get(self, request, *args, **kwargs):
        logout(request)
        return super(LogoutView, self).get(request, *args, **kwargs)
