from django.contrib.auth.decorators import login_required
from django.urls import path
from .views import HomeView, RegisterView, OnlineOrderPageView, LogoutView, LoginView

app_name = 'users'

urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path('online_order_page', login_required(OnlineOrderPageView.as_view()), name='online_order'),
    path('register_user', RegisterView.as_view(), name='register_user'),
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
]

