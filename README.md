
## dependecies
* python 3.9
* Django 4.0.4
* sqlite



---

## Setup
* git clone https://gitlab.com/luispayaresj/icook_django_avanzado.git
* cd icook
* mkdir templates static 
* pipenv install
* pipenv shell
* configure Pycharm interpreter (if this is your preferred IDE)
* pip install -r requirements_local.txt
* python manage.py migrate
* python manage.py createsuperuser
* python manage.py runserver

* Note: The db.sqlite3 database is uploaded because there are elements there without which the manual tests of the interface would not work
---

## Apps
### Orders
### Users

---

## Subjects:
### User authentication
### Class based views and Function based views 
### ORM. 


